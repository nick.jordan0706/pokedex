import 'package:flutter/material.dart';
import './screens/dashboard.dart';
import './screens/pokemonDetails.dart';
import './utils/constants.dart';
import './utils/my_theme.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'POKEDEX',
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        primarySwatch: Colors.blue,
        textTheme: MyTheme.textTheme,
      ),
      routes: <String, WidgetBuilder>{
        Constants.DASHBOARD_SCREEN: (BuildContext context) => DashboardLanding(),
      },
      initialRoute: Constants.DASHBOARD_SCREEN,
    );
  }
}
