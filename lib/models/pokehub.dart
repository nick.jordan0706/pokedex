import './pokemon.dart';

class PokeHub {
  List<Pokemon> pokemon;

  PokeHub.fromJson(Map<String,dynamic> parsedJson) {
    if (parsedJson['pokemon'] != null) {
      pokemon = new List<Pokemon>();
      parsedJson['pokemon'].forEach((v) {
        pokemon.add(new Pokemon.fromJson(v));
      });
    }
  }

  Map<String,dynamic> toJson() {
    final Map<String,dynamic> data = new Map<String,dynamic>();
    if (this.pokemon != null) {
      data['pokemon'] = this.pokemon.map((v) => v.toJson()).toList();
    }
    return data;
  }
}