import 'package:flutter/material.dart';
import 'package:pokedex/models/next_evolution.dart';

import '../models/pokemon.dart';

class PokemonDetails extends StatelessWidget {
  final Pokemon pokemon;
  final NextEvolution nextEvolution;

  PokemonDetails({this.pokemon, this.nextEvolution});

  Widget _buildPokemonDetails(BuildContext context) {
    return Stack(
      children: <Widget>[
        Positioned(
          height: MediaQuery.of(context).size.height / 1.5,
          width: MediaQuery.of(context).size.width - 20,
          left: 10.0,
          top: MediaQuery.of(context).size.height * 0.1,
          child: Card(
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(15.0),
            ),
            child: Column(
              children: <Widget>[
                SizedBox(
                  height: 40.0,
                ),
                Text(
                  pokemon.name,
                  style: TextStyle(
                    fontSize: 25.0,
                    fontWeight: FontWeight.bold,
                  ),
                ),
                Text('Height: ${pokemon.height}'),
                Text('Weight: ${pokemon.weight}'),
                SizedBox(height: 10),
                Text(
                  'Types',
                  style: TextStyle(fontWeight: FontWeight.bold),
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: pokemon.type
                      .map((t) => FilterChip(
                          backgroundColor: Colors.amber,
                          label: Text(
                            t,
                          ),
                          onSelected: (b) {}))
                      .toList(),
                ),
                SizedBox(height: 20),
                Text(
                  'Weaknesses',
                  style: TextStyle(fontWeight: FontWeight.bold),
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: pokemon.weaknesses
                      .map((t) => FilterChip(
                          backgroundColor: Colors.red,
                          label: Text(
                            t,
                            style: TextStyle(color: Colors.white),
                          ),
                          onSelected: (b) {}))
                      .toList(),
                ),
                SizedBox(
                  height: 10,
                ),
                Text(
                  'Next Evolution',
                  style: TextStyle(fontWeight: FontWeight.bold),
                ),
                SizedBox(height: 10),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: pokemon.nextEvolution == null
                      ? <Widget>[
                          Text(
                            'This is the final form',
                            style: TextStyle(fontWeight: FontWeight.w400),
                          )
                        ]
                      : pokemon.nextEvolution
                          .map((n) => FilterChip(
                                label: Text(n.name),
                                onSelected: (b) {},
                              ))
                          .toList(),
                )
              ],
            ),
          ),
        ),
        Align(
          alignment: Alignment.topCenter,
          child: Hero(
            tag: pokemon.img,
            child: Container(
              height: 100.0,
              width: 100.0,
              decoration: BoxDecoration(
                  image: DecorationImage(
                      fit: BoxFit.cover, image: NetworkImage(pokemon.img))),
            ),
          ),
        ),
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.cyan,
      appBar: AppBar(
        backgroundColor: Colors.cyan,
        elevation: 0.0,
        title: Text(pokemon.name),
      ),
      body: _buildPokemonDetails(context),
    );
  }
}
