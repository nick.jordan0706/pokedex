class Constants {
  static const DASHBOARD_SCREEN = '/dashboard';
  static const POKEMON_DETAILS = '/pokemondetails';

  static const API_BASE_URL = 'https://raw.githubusercontent.com/Biuni/PokemonGO-Pokedex/master/pokedex.json';
//   String apiPokemonList = 'https://pokedexvuejs.herokuapp.com/pokedexdb';
//   String apipokemonDetail = 'pokemon/';
//   String apiPokemonSpecies = 'pokemon-species/';
//   String apiPokemonMoves = 'move/';
}
